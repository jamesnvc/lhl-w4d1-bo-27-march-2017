//
//  FlickrPhoto.m
//  catsbreakout
//
//  Created by James Cash on 27-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "FlickrPhoto.h"

@implementation FlickrPhoto

- (instancetype)initWithAPIData:(NSDictionary*)dict
{
    self = [super init];
    if (self) {
        _flickrId = dict[@"id"];
        _title = dict[@"title"];
    }
    return self;
}


@end
