//
//  ViewController.m
//  catsbreakout
//
//  Created by James Cash on 27-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "FlickrAPI.h"

@interface ViewController ()

@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) NSArray *thingsToShow;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)performQuery {
    NSString *query = @"dog";
    [FlickrAPI searchFor:query
       completionHandler:^(NSArray<FlickrPhoto *> * photos) {
           NSLog(@"This happens second");
           self.thingsToShow = photos;
           [self.collectionView reloadData];
       }];
    NSLog(@"this happens first");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
