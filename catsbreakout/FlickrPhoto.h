//
//  FlickrPhoto.h
//  catsbreakout
//
//  Created by James Cash on 27-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrPhoto : NSObject

@property (nonatomic,strong) NSString *flickrId;
@property (nonatomic,strong) NSString *title;

- (instancetype)initWithAPIData:(NSDictionary*)dict;

@end
