//
//  FlickrAPI.h
//  catsbreakout
//
//  Created by James Cash on 27-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrPhoto.h"

@interface FlickrAPI : NSObject

+ (void)searchFor:(NSString*)query completionHandler:(void(^)(NSArray<FlickrPhoto*>*))done;

+ (void)loadImageForPhoto:(FlickrPhoto*)photo completionHandler:(void(^)(UIImage* img))done;

@end
