//
//  FlickrAPI.m
//  catsbreakout
//
//  Created by James Cash on 27-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "FlickrAPI.h"
#import "ApiKeys.h"

@implementation FlickrAPI

+ (void)searchFor:(NSString *)query completionHandler:(void (^)(NSArray<FlickrPhoto *> *))loadedSearchResultsBlock
{
    NSURL* queryURL =
    [NSURL
     URLWithString:[NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&api_key=%@&tags=%@", FLICKR_API_KEY, query]];
    NSLog(@"creating task");
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:queryURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"Task complete");
        // check for errors
        // Use NSJSONSerializer to turn our NSData into an array of FlickrPhoto
        // for (dict in responseInfo[@"photos"][@"photo"] {
        //      [[FlickrPhoto alloc] initWithAPIData:dict];
        // }
        NSLog(@"Calling results block");
        loadedSearchResultsBlock(@[// array would actually have all the FlickPhoto objects made above
               // in the same way as the github example this morning
               ]);
    }];
    [task resume];
    NSLog(@"Started task");
}

+ (NSURL *)URLForPhoto:(FlickrPhoto*)photo
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://farm%@.whatever/%@.biz", photo.flickrId, photo.title]];
}

+ (void)loadImageForPhoto:(FlickrPhoto *)photo completionHandler:(void (^)(UIImage *))imageLoadedBlock
{
    NSURLSessionTask *task = [[NSURLSession sharedSession]
                              dataTaskWithURL:[FlickrAPI URLForPhoto:photo]
                              completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                  // Check for error here!
                                  UIImage *img = [UIImage imageWithData:data];
                                  // maybe dispatch this to the main thread
                                  // because it's probably going to want to do some UI things with this
                                  // also reasonable to just let the caller handle that
                                  [[NSOperationQueue mainQueue]
                                   addOperationWithBlock:^{
                                       imageLoadedBlock(img);
                                   }];
                                  // in the collection view cell
                                  // [FlickrAPI loadImage...: completion:^(UIImage *img){ self.image = img; }];
                              }];
    [task resume];
}

@end
